//
//  HomeViewController.swift
//  LGSideMenu
//
//  Created by MAC on 20/05/19.
//  Copyright © 2019 Vishal. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func menuButtonClick(_ sender: UIButton) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}

