//
//  RightViewController.swift
//  LGSideMenu
//
//  Created by MAC on 21/05/19.
//  Copyright © 2019 Vishal. All rights reserved.
//

import UIKit

class RightViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func homeButtonClick(_ sender: UIButton) {
        let mainViewController = sideMenuController!
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.setViewControllers([vc], animated: false)
        mainViewController.hideRightView(animated: true, completionHandler: nil)
    }
    @IBAction func otherButtonClick(_ sender: UIButton) {
        let mainViewController = sideMenuController!
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "OtherViewController") as! OtherViewController
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.setViewControllers([vc], animated: false)
        mainViewController.hideRightView(animated: true, completionHandler: nil)
    }
}
